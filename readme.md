#Sukurti žaidimą "Atspėk skaičių", naudojant EmberJS karkasą

#Žaidimo taisyklės:
 * Kompiuteris sugalvoja skaičių X nuo 1 iki 10.
 * Vartotojas spėja skaičių nuo 1 iki 10, o kompiuteris pasako ar skaičius X yra didesnis ar mažesnis už vartotojo spėjimą
 * Spėliojama tiek kartų, kol vartotojas atspėja sugalvotą skaičių X
 * Atspėjus skaičių, kompiuteris pasako per kiek spėjimų skaičius buvo atspėtas

 * Atspėjus skaičių, žaidimą galima pradėti iš naujo

#Reikalavimai:
 * Žaidimas turi būti sukurtas naudojant Javascript su EmberJS (2.10 versija, http://emberjs.com/)
 * Kodą (nedidelėmis iteracijomis) talpinti Github/Bitbucket viešos prieigos kodo saugykloje (public repository) (pradėjus užduotį, atsiųsti linką į saugyklą)

#Bonus balai:
 * Žaidimas ištestuotas (idealiausiu atveju unit, integration, acceptance testai)