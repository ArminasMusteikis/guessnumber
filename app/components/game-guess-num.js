import Ember from 'ember';
var random; //ar bloga praktika kurti global variables?
var counter = 0;
var guess = 0;
export default Ember.Component.extend({
    isGameStarted: false,
    isGameOver: false,
    guessArr: Ember.A(),
    counterAnswer: Ember.computed('guess', function(){
        return counter;
    }),
    lowerHigher: Ember.computed('guessArr.[]', function(){
        console.log(random); // parodo console koks yra sugeneruotas skaicius
        if(guess == 0){                        
        }
        else if(guess < random){
            return "Atsakymas yra didesnis";
        }  
        else if(guess > random){
            return "Atsakymas yra mažesnis";
        }    
    }),
    actions: {       
        startGame: function(){
            counter = 0;
            guess = 0;
            random = Math.ceil(Math.random() * 10);
            this.set('random', random);
            this.set('isGameStarted', true);
        },    
        restartGame: function(){
            counter = 0;
            guess = 0;
            this.set('guessArr', []);           
            random = Math.ceil(Math.random() * 10);
            this.set('random', random);
            this.set('isGameOver', false);
        },
        isCorrect: function(){
            guess = this.get('guess');
            counter++;
            this.get('guessArr').addObject(counter);      
            if(guess == random){
                this.set('isGameOver', true);
            }           
        },
    }
});
